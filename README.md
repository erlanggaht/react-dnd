# Tasks - React Beatiful DND

**Preview OneTask :**
![Feature : oneTask](https://i.ibb.co/HCD8TZ9/Animation.gif)

**Preview MultiTask :**
![Feature : oneTask](https://i.ibb.co/KK1FGg9/Animation.gif)

**Cara Install :**

- install package _npm i react-beautiful-dnd_
- untuk fitur satu column ada dibranch _feat/onecolumn_
- untuk fitur multi column ada dibranch _develop_

**Cara Lihat Fitur One Task** :

- clone repository ini
- git checkout **feat/onecolumn**
- npm run dev

**Cara Lihat Fitur Multi Task** :

- clone repository ini
- git checkout **develop**
- npm run dev

#### Terminologi

- **Droppable**  Membungkus bagian aplikasi yang ingin Anda aktifkan drag and dropnya
- **Droppable** Area yang bisa dijatuhkan. Mengandung **<Draggable />**>S
- **Draggable**  Apa yang bisa diseret
- **resetServerContext()**- Utilitas untuk rendering sisi server (SSR)

#### Props

<DragDropContext /> onDragStart, onDragUpdate, onDragEnd and onBeforeDragStart

#### Fitur - ONE TASK

- Buat InitialTask, export object tersebut untuk di consume :

```js
const initialTasks = [
  {
    id: 1,
    title: "Item 1",
  },
  {
    id: 2,
    title: "Item 2",
  },
  {
    id: 3,
    title: "Item 3",
  },
];
```

- Masukan kedalam file component. contoh file app.jsx

```js
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

export default function App() {

// Logic Simpan Disini

return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="tasks" type="TASK1">
        {(provided) => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            {tasks.map((item, index) => (
              <Draggable
                key={item.id}
                draggableId={item.id.toString()}
                index={index}
              >
                {(provided, snapshot) => {
                  const style = {
                    ...provided.draggableProps.style,
                    backgroundColor: snapshot.isDragging ? "#38bdf8" : "white",
                    color: snapshot.isDragging ? "white" : "#222",
                    fontSize: 18,
                  };
                  return (
                    <div
                      className="Item  p-2 rounded mt-1 "
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      ref={provided.innerRef}
                      style={style}
                    >
                      <p>{item.title}</p>
                    </div>
                  );
                }}
              </Draggable>
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
}
```

- untuk **LOGIC** fungsi OnDragEnd dari DragDropContext.
  note : fungsi ini untuk membuat logic drag and drop data dari initial state atau dari data **initialTasks**

```js
import { useState } from "react";

const [tasks, setTasks] = useState(initialTasks);

const onDragEnd = (result) => {
  console.log(result); // untuk melihat return callback dari onDragEnd

  if (!result.destination) return;

  const items = Array.from(tasks);
  const [reorderedItem] = items.splice(result.source.index, 1);
  items.splice(result.destination.index, 0, reorderedItem);

  setTasks(items);
};
```

- jika digabungkan semua script js diatas menjadi seperti ini :

```js
import { useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

const initialTasks = [
  {
    id: 1,
    title: "Item 1",
  },
  {
    id: 2,
    title: "Item 2",
  },
  {
    id: 3,
    title: "Item 3",
  },
];

function App() {
  const [tasks, setTasks] = useState(initialTasks);

  const onDragEnd = (result) => {
    console.log(result);

    if (!result.destination) return;

    const items = Array.from(tasks);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);

    setTasks(items);
  };

  return (
    <>
      <h1 className="text-center my-6">React Beautiful DND - Erlangga</h1>
      <main className="Area-Task bg-gray-200 rounded shadow-sm p-6 flex gap-4">
        <div className="Task1 border-2 border-[#ccc] bg-gray-100 p-3 rounded w-56">
          <h1 className="bg-white shadow text-center p-3 rounded mb-3 rounded-b-none">
            Task 1
          </h1>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="tasks" type="TASK1">
              {(provided) => (
                <div ref={provided.innerRef} {...provided.droppableProps}>
                  {tasks.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id.toString()}
                      index={index}
                    >
                      {(provided, snapshot) => {
                        const style = {
                          ...provided.draggableProps.style,
                          backgroundColor: snapshot.isDragging
                            ? "#38bdf8"
                            : "white",
                          color: snapshot.isDragging ? "white" : "#222",
                          fontSize: 18,
                        };
                        return (
                          <div
                            className="Item  p-2 rounded mt-1 "
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            ref={provided.innerRef}
                            style={style}
                          >
                            <p>{item.title}</p>
                          </div>
                        );
                      }}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </div>
      </main>
    </>
  );
}

export default App;
```

#### Fitur - Multi TASK

- Buat initialTask, export object tersebut untuk di consume :

```js
let initialTasks = [
  {
    id: 1,
    title: "Item 1",
  },
  {
    id: 2,
    title: "Item 2",
  },
  {
    id: 3,
    title: "Item 3",
  },
];
```

- Buat file component, contoh di file app.jsx

```js

import { useState } from "react";
import { DragDropContext } from "react-beautiful-dnd";

export default function App() {

  // LOGIC SIMPAN DISINI

 return ( 
  <form
        className="bg-gray-100 p-3 flex mt-12 items-center justify-between"
        onSubmit={(e) => handleSubmit(e)}
      >
        <div className="">
          <button className="bg-white rounded shadow ml-2 p-2">
            Tambah Item
          </button>
          <button
            type="button"
            className="bg-white rounded shadow ml-2 p-2"
            onClick={() => addHead()}
          >
            Tambah Task
          </button>
        </div>
        <button
          type="button"
          className="bg-sky-500 text-white rounded shadow ml-2 p-2"
          onClick={() => location.reload()}
        >
          Reload Page
        </button>
      </form>
      <main className="Area-Task bg-gray-200 rounded shadow-sm p-6 flex flex-col lg:flex-row items-center lg:items-start lg:flex-wrap gap-4 justify-center  ">
    
      {/* Inti React Beatiful DND */}

        <DragDropContext
          onDragEnd={(result) => onDragEnd(result, tasks, setTasks)}
        >
          <RBD tasks={tasks}  setTasks={setTasks}  />
        </DragDropContext>

     {/* Inti React Beatiful DND */}

      </main>
 )}

```

untuk component RBD, dokumentasi nya baca kebawah.

- Buat State react menggunakan useState untuk konfigurasi data :
  note : masukan kedalam component app tadi didalam Logic

```js
let [num, setNum] = useState(4);
let [numItem, setNumItem] = useState(4);
const [tasks, setTasks] = useState({
  1: {
    title: "Task 1",
    items: initialTasks,
  },
  2: {
    title: "Task 2",
    items: [],
  },
  3: {
    title: "Task 3 ",
    items: [],
  },
});
```

STATE**num** : dipakai untuk logic menambah id head/task dan title nomor task
**numItem** :  dipakai untuk logic menambah id item dan title nomor item
**tasks** : dipakai untuk menyimpan initialTask/initialState kedalam useState, agar bisa mengatur logic react beatiful dnd didalam setTasks langsung.

- untuk Logic fungsi onDragEnd, simpan didalam LOGIC App.jsx

```js
const onDragEnd = (result, tasks, setTasks) => {
  if (!result.destination) return;

  const { source, destination } = result;
  if (source.droppableId !== destination.droppableId) {
    const sourceColumn = tasks[source.droppableId];
    const destColumn = tasks[destination.droppableId];
    const sourceItems = [...sourceColumn.items];
    const destItems = [...destColumn.items];
    const [removed] = sourceItems.splice(source.index, 1);
    destItems.splice(destination.index, 0, removed);
    setTasks({
      ...tasks,
      [source.droppableId]: {
        ...sourceColumn,
        items: sourceItems,
      },
      [destination.droppableId]: {
        ...destColumn,
        items: destItems,
      },
    });
  } else {
    const column = tasks[source.droppableId];
    const copiedItems = [...column.items];
    const [removed] = copiedItems.splice(source.index, 1);
    copiedItems.splice(destination.index, 0, removed);
    setTasks({
      ...tasks,
      [source.droppableId]: {
        ...column,
        items: copiedItems,
      },
    });
  }
};
```

- HandleSubmit, fungsi submit tambah item

```js
const handleSubmit = (e) => {
  e.preventDefault();
  if (numItem === 50) return alert("maaf, batas 50 item");
  let newData = {
    id: numItem,
    title: `item ${numItem}`,
  };

  const task1 = tasks["1"];
  const task1ItemsArray = Object.values(task1.items);
  setTasks({
    ...tasks,
    1: {
      ...task1,
      items: [...task1ItemsArray, newData],
    },
  });

  setNumItem(numItem + 1);
};
```

- AddHead, untuk tambah task baru :

```js
const addHead = () => {
  if (num === 9) return alert("maaf, batas 8 tasks");
  setTasks({
    ...tasks,
    [num]: {
      title: `Tasks ${num} `,
      items: [],
    },
  });
  setNum(num + 1);
};
```

**RBD**

RBD ini bukan component dari react beatiful tetapi component item dan task secara terpisah.

- buat component baru atau file baru. misal dengan nama rbd.jsx
- Component RBD, - jangan lupa masukan props { tasks, setTasks } pada component rbd

```js
import deleteIcon from "../assets/delete.svg";

export default RBD ({tasks,setTasks}) {
  
  // Logic Simpan Disini


return (
  <>
  {Object.entries(tasks).map(([columnId, column]) => {
    return (
      <Droppable key={columnId} droppableId={columnId} type="ITEM">
        {(provided) => (
          <div
            ref={provided.innerRef}
            {...provided.droppableProps}
            className="bg-gray-100 w-full lg:w-96 p-2 "
          >
            <h1 className="shadow bg-white p-4 text-center text-sky-500 relative">
              <img
                src={deleteIcon}
                width={24}
                height={24}
                alt="delete"
                className={`absolute right-2 cursor-pointer hover:opacity-80 bg-red-500 rounded top-2 ${
                  column.title === "Task 1" && "hidden"
                }`}
                onClick={() => deleteHead(column.title)}
              />
              {column.title}
            </h1>
            <div className="max-h-[320px] overflow-auto">
              {column.items.map((item, index) => (
                <Draggable
                  key={item.id}
                  draggableId={item.id.toString()}
                  index={index}
                  type="ITEM"
                >
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      className=" mt-1 rounded py-2 px-4 flex justify-between items-center border-2 border-transparent bg-gray-50 hover:border-sky-500 transition-all hover:shadow-inner  shadow"
                      style={getStyle(provided.draggableProps.style, snapshot)}
                    >
                      <p className="text-lg text-[#777]">{item.title}</p>
                      <p className="text-sm  text-[#777] p-2 rounded">
                        {column.title}
                      </p>

                      <div className="flex gap-2">
                        <p
                          className="text-sm bg-red-500 cursor-pointer hover:opacity-90 text-white p-2 rounded"
                          onClick={() => deleteItem(item.id, columnId)}
                        >
                          hapus
                        </p>
                      </div>
                    </div>
                  )}
                </Draggable>
              ))}
            </div>
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    );
  })}
</>
)}

```

- untuk fungsi logic delete item :

```js
const deleteItem = (itemId, columnId) => {
  setTasks((prev) => {
    const filterdata = prev[columnId]["items"].filter(
      (item) => item.id !== itemId
    );
    return {
      ...prev,
      [columnId]: {
        ...prev[columnId],
        items: filterdata,
      },
    };
  });
};
```

- untuk fungsi delete task :

```js
const deleteHead = (title) => {
  const taskArray = Object.values(tasks);
  const dataDelete = taskArray.filter((m) => m.title !== title);
  setTasks(dataDelete);
};
```

- untuk fungsi style dan mengatur style React Beatiful DND :

```js
const getStyle = (style, snapshot) => {
  if (!snapshot.isDropAnimating) {
    return style;
  }

  return {
    ...style,
    transition: `all .2s`,
    opacity: "0.2",
  };
};
```

- Jika digabungkan kedalam satu component App.jsx :

```js
import { useState } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import { Draggable, Droppable } from "react-beautiful-dnd";
import deleteIcon from "../assets/delete.svg";

let initialTasks = [
  {
    id: 1,
    title: "Item 1",
  },
  {
    id: 2,
    title: "Item 2",
  },
  {
    id: 3,
    title: "Item 3",
  },
];

export default function App() {
  const [initialTasks] = useState(initialTasks);

  let [num, setNum] = useState(4);
  let [numItem, setNumItem] = useState(4);
  const [tasks, setTasks] = useState({
    1: {
      title: "Task 1",
      items: initialTasks,
    },
    2: {
      title: "Task 2",
      items: [],
    },
    3: {
      title: "Task 3 ",
      items: [],
    },
  });

  const onDragEnd = (result, tasks, setTasks) => {
    if (!result.destination) return;

    const { source, destination } = result;
    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = tasks[source.droppableId];
      const destColumn = tasks[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setTasks({
        ...tasks,
        [source.droppableId]: {
          ...sourceColumn,
          items: sourceItems,
        },
        [destination.droppableId]: {
          ...destColumn,
          items: destItems,
        },
      });
    } else {
      const column = tasks[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setTasks({
        ...tasks,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (numItem === 50) return alert("maaf, batas 50 item");
    let newData = {
      id: numItem,
      title: `item ${numItem}`,
    };

    const task1 = tasks["1"];
    const task1ItemsArray = Object.values(task1.items);
    setTasks({
      ...tasks,
      1: {
        ...task1,
        items: [...task1ItemsArray, newData],
      },
    });

    setNumItem(numItem + 1);
  };

  // Tambah Area Item
  const addHead = () => {
    if (num === 9) return alert("maaf, batas 8 tasks");
    setTasks({
      ...tasks,
      [num]: {
        title: `Tasks ${num} `,
        items: [],
      },
    });
    setNum(num + 1);
  };

  return (
    <>
      <form
        className="bg-gray-100 p-3 flex mt-12 items-center justify-between"
        onSubmit={(e) => handleSubmit(e)}
      >
        <div className="">
          <button className="bg-white rounded shadow ml-2 p-2">
            Tambah Item
          </button>
          <button
            type="button"
            className="bg-white rounded shadow ml-2 p-2"
            onClick={() => addHead()}
          >
            Tambah Task
          </button>
        </div>
        <button
          type="button"
          className="bg-sky-500 text-white rounded shadow ml-2 p-2"
          onClick={() => location.reload()}
        >
          Reload Page
        </button>
      </form>
      <main className="Area-Task bg-gray-200 rounded shadow-sm p-6 flex flex-col lg:flex-row items-center lg:items-start lg:flex-wrap gap-4 justify-center  ">
        <DragDropContext
          onDragEnd={(result) => onDragEnd(result, tasks, setTasks)}
        >
          <RBD tasks={tasks} setTasks={setTasks} />
        </DragDropContext>
      </main>
      {/* <Background/> */}
    </>
  );
}

export function RBD({ tasks, setTasks }) {
  const deleteItem = (itemId, columnId) => {
    setTasks((prev) => {
      const filterdata = prev[columnId]["items"].filter(
        (item) => item.id !== itemId
      );
      return {
        ...prev,
        [columnId]: {
          ...prev[columnId],
          items: filterdata,
        },
      };
    });
  };

  const deleteHead = (title) => {
    const taskArray = Object.values(tasks);
    const dataDelete = taskArray.filter((m) => m.title !== title);
    setTasks(dataDelete);
  };

  const getStyle = (style, snapshot) => {
    if (!snapshot.isDropAnimating) {
      return style;
    }
    // patching the existing style
    return {
      ...style,
      transition: `all .2s`,
      opacity: "0.2",
    };
  };

  return (
    <>
      {Object.entries(tasks).map(([columnId, column]) => {
        return (
          <Droppable key={columnId} droppableId={columnId} type="ITEM">
            {(provided) => (
              <div
                ref={provided.innerRef}
                {...provided.droppableProps}
                className="bg-gray-100 w-full lg:w-96 p-2 "
              >
                <h1 className="shadow bg-white p-4 text-center text-sky-500 relative">
                  <img
                    src={deleteIcon}
                    width={24}
                    height={24}
                    alt="delete"
                    className={`absolute right-2 cursor-pointer hover:opacity-80 bg-red-500 rounded top-2 ${
                      column.title === "Task 1" && "hidden"
                    }`}
                    onClick={() => deleteHead(column.title)}
                  />
                  {column.title}
                </h1>
                <div className="max-h-[320px] overflow-auto">
                  {column.items.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id.toString()}
                      index={index}
                      type="ITEM"
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          className=" mt-1 rounded py-2 px-4 flex justify-between items-center border-2 border-transparent bg-gray-50 hover:border-sky-500 transition-all hover:shadow-inner  shadow"
                          style={getStyle(
                            provided.draggableProps.style,
                            snapshot
                          )}
                        >
                          <p className="text-lg text-[#777]">{item.title}</p>
                          <p className="text-sm  text-[#777] p-2 rounded">
                            {column.title}
                          </p>

                          <div className="flex gap-2">
                            <p
                              className="text-sm bg-red-500 cursor-pointer hover:opacity-90 text-white p-2 rounded"
                              onClick={() => deleteItem(item.id, columnId)}
                            >
                              hapus
                            </p>
                          </div>
                        </div>
                      )}
                    </Draggable>
                  ))}
                </div>
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        );
      })}
    </>
  );
}
```
