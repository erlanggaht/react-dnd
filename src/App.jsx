import { useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

const initialTasks = [
  {
    id: 1,
    title: "Item 1",
  },
  {
    id: 2,
    title: "Item 2",
  },
  {
    id: 3,
    title: "Item 3",
  },
];

function App() {
  const [tasks, setTasks] = useState(initialTasks);

  const onDragEnd = (result) => {
    console.log(result);

    if (!result.destination) return;

    const items = Array.from(tasks);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);

    setTasks(items);
  };

  return (
    <>
      <h1 className="text-center my-6">React Beautiful DND - Erlangga</h1>
      <main className="Area-Task bg-gray-200 rounded shadow-sm p-6 flex gap-4">
        <div className="Task1 border-2 border-[#ccc] bg-gray-100 p-3 rounded w-56">
          <h1 className="bg-white shadow text-center p-3 rounded mb-3 rounded-b-none">Task 1</h1>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="tasks" type="TASK1">
              {(provided) => (
                <div ref={provided.innerRef} {...provided.droppableProps}>
                  {tasks.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id.toString()}
                      index={index}
                    >
                      {(provided, snapshot) => {
                        const style = {
                          ...provided.draggableProps.style,
                          backgroundColor: snapshot.isDragging
                            ? "#38bdf8"
                            : "white",
                          color: snapshot.isDragging ? "white" : "#222",
                          fontSize: 18,
                        };
                        return (
                          <div
                            className="Item  p-2 rounded mt-1 "
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            ref={provided.innerRef}
                            style={style}
                          >
                            <p>{item.title}</p>
                          </div>
                        );
                      }}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </div>
      </main>
    </>
  );
}

export default App;
